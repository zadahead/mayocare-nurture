import numpy as np
import joblib
import json

def sign(x):
    a = "high"
    b = "moderate"
    c = "low"
    if x>0:
        y=a
    elif x<0:
        y=c
    else:
        y=b
    return y

def add_to_dict(dict, key, value):
    if key in dict:
        dict[key].append(value)
    else:
        dict[key] = [value]


def main(event):

    ad_dict={}
    fa_dict={}

    # variables
    highTEWL = 12
    midTEWL = 7
    lowTEWL = 5
    winter = [1, 2, 3, 9, 10, 11, 12]
    high_weigh = 4000
    low_weigh = 2500
    FA_gest = 37
    AD_gest = 36
    mom_age = 29

    # scores:
    ad_fa_gest=-50
    fa_no_BF= -60
    fa_BF= 80
    ad_fa_no_caeserian= -2
    ad_fa_caeserian= 2
    ad_fa_no_cat= -9
    ad_fa_childcare= -5
    ad_fa_summer = -21
    ad_fa_winter = 21
    ad_fa_dog= -30
    ad_fa_asthma_in_family= 80
    fa_atopic_in_family= 80
    ad_sib_atopic= 90
    ad_paternal_atopic= 100
    ad_family_allergy= 50
    fa_family_allergy= 100
    ad_fa_family_fever= 80
    ad_fa_male= -1
    ad_fa_female= 1
    ad_fa_mom_smoke= 15
    ad_fa_mom_age= -5
    ad_fa_farm= -15
    ad_fa_more_than_1_sib= -6
    ad_fa_non_white = 25
    ad_fa_tewl_under_5= -20
    ad_fa_tewl_7_to_12 = 20
    ad_fa_tewl_above_12= 40
    ad_fa_rural = -39
    ad_fa_vitaminD = -4
    ad_fa_weight_above_4000= 20

    no_add=0

    #event to variables
    TEWL=event["TEWL"]
    month=event["month"]
    day=event["day"]
    skintype=event["skin"]
    weight=event["weigh"]
    gest_age=event["gestational"]
    gender=event["gender"]
    caesarian=event["isCaesarean"]

    #mom
    if "atopicDermatitis" in event["momCondition"]: ADmom = True
    else: ADmom = False
    if "hayFever" in event["momCondition"]: fever_mom = True
    else: fever_mom = False
    if "asthma" in event["momCondition"]: asthma_mom = True
    else: asthma_mom = False
    if "foodAllergy" in event["momCondition"]: FAmom = True
    else: FAmom = False
    #father
    if "atopicDermatitis" in event["fatherCondition"]: ADdad = True
    else: ADdad = False
    if "hayFever" in event["fatherCondition"]: fever_dad = True
    else: fever_dad = False
    if "asthma" in event["fatherCondition"]: asthma_dad = True
    else: asthma_dad = False
    if "foodAllergy" in event["fatherCondition"]: FAdad = True
    else: FAdad = False
    #sibilings
    if "hayFever" in event["siblingsCondition"]: fever_sibiling = True
    else: fever_sibiling = False
    if "foodAllergy" in event["siblingsCondition"]: FAsibiling = True
    else: FAsibiling = False
    if "atopicDermatitis" in event["siblingsCondition"]: ADsibiling = True
    else: ADsibiling = False
    if "asthma" in event["siblingsCondition"]: asthma_sibiling = True
    else: asthma_sibiling = False    

    hay_fever_in_family= fever_sibiling or fever_dad or fever_mom
    asthma_in_family= asthma_sibiling or asthma_dad or asthma_mom
    atopic_parental = ADmom or ADdad
    atopic_sib= ADsibiling
    fa_in_family= FAmom or FAdad or FAsibiling

    farm=event["isMomExposedFarmAnimals"]
    m_age=event["momBirth"]
    num_sibiling=event["siblings"]
    dog=event["isDog"]
    cat=event["isCat"]
    residence=event["liveSetting"]
    vit_d= event["includeVitaminD"]
    breastfeeding=event["isBreastfeed"]
    child_care=event["childCareTimeStart"]
    mom_smoke=event["isMomSmoke"]

    # TEWL
    if TEWL > highTEWL:
        add_to_dict(ad_dict,"skin", ad_fa_tewl_above_12 )
        add_to_dict(fa_dict,"skin", ad_fa_tewl_above_12 )
    elif TEWL > midTEWL:
        add_to_dict(ad_dict, "skin", ad_fa_tewl_7_to_12)
        add_to_dict(fa_dict, "skin", ad_fa_tewl_7_to_12)
    elif TEWL > lowTEWL:
        add_to_dict(ad_dict, "skin", no_add)
        add_to_dict(fa_dict, "skin", no_add)
    else:
        add_to_dict(ad_dict, "skin", ad_fa_tewl_under_5)
        add_to_dict(fa_dict, "skin", ad_fa_tewl_under_5)


    # DOB
    if month in winter:
        if month in [1, 2, 10, 11, 12]:
            add_to_dict(ad_dict, "infant", ad_fa_winter)
            add_to_dict(fa_dict, "infant", ad_fa_winter)
        elif month == 3:
            if day < 22:
                add_to_dict(ad_dict, "infant", ad_fa_winter)
                add_to_dict(fa_dict, "infant", ad_fa_winter)
                # AD_score.append(winter_score)
                # FA_score.append(winter_score)
        elif month == 9:
            if day > 21:
                add_to_dict(ad_dict, "infant", ad_fa_winter)
                add_to_dict(fa_dict, "infant", ad_fa_winter)
        else:
            add_to_dict(ad_dict, "infant", ad_fa_summer)
            add_to_dict(fa_dict, "infant", ad_fa_summer)
    else:
        add_to_dict(ad_dict, "infant", ad_fa_summer)
        add_to_dict(fa_dict, "infant", ad_fa_summer)

    # weight
    if weight > high_weigh:
        add_to_dict(ad_dict, "infant", ad_fa_weight_above_4000)
        add_to_dict(fa_dict, "infant", ad_fa_weight_above_4000)
    else:
        add_to_dict(ad_dict, "infant", no_add)
        add_to_dict(fa_dict, "infant", no_add)

    # gest_age
    if gest_age <= AD_gest:
        add_to_dict(fa_dict, "pregnancy", ad_fa_gest)
        add_to_dict(ad_dict, "pregnancy", ad_fa_gest)
    else:
        add_to_dict(ad_dict, "pregnancy", no_add)
        add_to_dict(fa_dict, "pregnancy", no_add)

    # gender
    if gender == "Male":
        add_to_dict(fa_dict, "infant", ad_fa_male)
        add_to_dict(ad_dict, "infant", ad_fa_male)
    else:
        add_to_dict(fa_dict, "infant", ad_fa_female)
        add_to_dict(ad_dict, "infant", ad_fa_female)

    # atopy in family
    if atopic_parental :
        add_to_dict(ad_dict, "history", ad_paternal_atopic)
    elif atopic_sib:
        add_to_dict(ad_dict, "history", ad_sib_atopic )
    else:
        add_to_dict(ad_dict, "history", no_add)

    if (atopic_parental) | (atopic_sib):
        add_to_dict(fa_dict, "history", fa_atopic_in_family)
    else:
        add_to_dict(fa_dict, "history", no_add)

    # Hay fever
    if hay_fever_in_family :
        add_to_dict(fa_dict, "history", hay_fever_in_family )
        add_to_dict(ad_dict, "history", hay_fever_in_family )
    else:
        add_to_dict(fa_dict, "history", no_add )
        add_to_dict(ad_dict, "history", no_add )

    # food allergy in family:
    if fa_in_family:
        add_to_dict(fa_dict, "history", fa_family_allergy)
        add_to_dict(ad_dict, "history", ad_family_allergy)
    else:
        add_to_dict(fa_dict, "history", no_add)
        add_to_dict(ad_dict, "history", no_add)

    # farm
    if farm:
        add_to_dict(ad_dict, "pregnancy", ad_fa_farm)
        add_to_dict(fa_dict, "pregnancy", ad_fa_farm)
    else:
        add_to_dict(fa_dict, "pregnancy", no_add)
        add_to_dict(ad_dict, "pregnancy", no_add)

    # mom's age
    if 2021 - m_age < mom_age:
        add_to_dict(fa_dict, "infant", ad_fa_mom_age)
        add_to_dict(ad_dict, "infant", ad_fa_mom_age)
    else:
        add_to_dict(fa_dict, "infant", no_add)
        add_to_dict(ad_dict, "infant", no_add)

    # Caesarian
    if caesarian:
        add_to_dict(fa_dict, "pregnancy", ad_fa_caeserian)
        add_to_dict(ad_dict, "pregnancy", ad_fa_caeserian)
    else:
        add_to_dict(fa_dict, "pregnancy", no_add)
        add_to_dict(ad_dict, "pregnancy", no_add)

    # sibilings
    if float(num_sibiling) == 0:
        add_to_dict(fa_dict, "settings", no_add)
        add_to_dict(ad_dict, "settings", no_add)
    else:
        add_to_dict(fa_dict, "settings", ad_fa_more_than_1_sib )
        add_to_dict(ad_dict, "settings", ad_fa_more_than_1_sib )

    # Settings
    if dog:
        add_to_dict(ad_dict, "settings", ad_fa_dog)
        add_to_dict(fa_dict, "settings", ad_fa_dog)
    else:
        add_to_dict(ad_dict, "settings", no_add)
        add_to_dict(fa_dict, "settings", no_add)

    if cat:
        add_to_dict(ad_dict, "settings", no_add)
        add_to_dict(fa_dict, "settings", no_add)
    else:
        add_to_dict(ad_dict, "settings", ad_fa_no_cat)
        add_to_dict(fa_dict, "settings", ad_fa_no_cat)

    if residence == "Rural":
        add_to_dict(ad_dict, "settings", ad_fa_rural)
        add_to_dict(fa_dict, "settings", ad_fa_rural)
    else:
        add_to_dict(ad_dict, "settings", no_add)
        add_to_dict(fa_dict, "settings", no_add)

    # Future
    if vit_d:
        add_to_dict(fa_dict, "future", ad_fa_vitaminD)
        add_to_dict(ad_dict, "future", ad_fa_vitaminD)
    else:
        add_to_dict(fa_dict, "future", no_add)
        add_to_dict(ad_dict, "future", no_add)

    if breastfeeding:
        add_to_dict(fa_dict, "future", fa_BF)
    else:
        add_to_dict(fa_dict, "future", fa_no_BF)
        # FA_score.apend(no_add)

    if child_care == "Month0" or child_care == "Month4":
        add_to_dict(fa_dict, "future", ad_fa_childcare)
        add_to_dict(ad_dict, "future", ad_fa_childcare)
    else:
        add_to_dict(fa_dict, "future", no_add)
        add_to_dict(ad_dict, "future", no_add)

    #smoke
    if mom_smoke:
        add_to_dict(fa_dict, "pregnancy", ad_fa_mom_smoke)
        add_to_dict(ad_dict, "pregnancy", ad_fa_mom_smoke)
    else:
        add_to_dict(fa_dict, "pregnancy", no_add)
        add_to_dict(ad_dict, "pregnancy", no_add)
      
    AD_model_path = "./models/ADmodel"
    AD_model = joblib.load(AD_model_path)
    FA_model_path = "./models/FAmodel"
    FA_model = joblib.load(FA_model_path)
    frame = []

    # gender- string
    if gender == "Male":
        frame.append(int(0))
    else:
        frame.append(int(1))

    # Gestational Age- int in weeks
    frame.append(event["gestational"])

    # mode of delivery- boolean
    if caesarian:
        frame.append(int(1))
    else:
        frame.append(int(0))

    # Birthweight- int ing grams
    frame.append(weight)

    # Mother Smoked During Pregnancy- boolean
    if mom_smoke:
        frame.append(int(1))
    else:
        frame.append(int(0))

    # Maternal AD- boolean
    if ADmom:
        frame.append(int(1))
    else:
        frame.append(int(0))

    # Maternal FA- boolean
    if FAmom:
        frame.append(int(1))
    else:
        frame.append(int(0))

    # Maternal asthma- boolean
    if asthma_mom:
        frame.append(int(1))
    else:
        frame.append(int(0))

    # Paternal AD- boolean
    if ADdad:
        frame.append(int(1))
    else:
        frame.append(int(0))

    # Paternal Food Allergy- boolean
    if FAdad:
        frame.append(int(1))
    else:
        frame.append(int(0))

    # Paternal Asthma- boolean
    if asthma_dad:
        frame.append(int(1))
    else:
        frame.append(int(0))

    # Excl. Breastfed (2 weeks)- need Denys help
    frame.append(int(0))

    # Excl. Breastfed (3 months)-boolean- not sure what is consider as true- boolean
    if breastfeeding:
        frame.append(int(0))
    else:
        frame.append(int(1))

    # Household Pets
    if (cat or dog):
        frame.append(int(1))
    else:
        frame.append(int(0))

    # ethnicity_0- string
    if skintype == "White":
        frame.append(int(1))
    else:
        frame.append(int(0))

    # ethnicity_1-
    if (skintype == "Chinese" or skintype == "Other" or skintype == "MixedEthnicity"):
        frame.append(int(1))
    else:
        frame.append(int(0))

    # ethnicity_2- string
    if skintype == "Asian":
        frame.append(int(1))
    else:
        frame.append(int(0))

    # ethnicity_3- string
    if skintype == "Black":
        frame.append(int(1))
    else:
        frame.append(int(0))

    # # Mixed ethnicity- string
    # if (skintype == "Chinese" or skintype == "Other"):
    #     frame.append(int(1))
    # else:
    frame.append(int(0))

    arr_frame=np.array(frame).reshape(1, 19)

    pred_ad = AD_model.predict(arr_frame) #numerical AD risk assessment

    #AD thresholds for each category (low/ medium/ high)
    # AD_thresh_low = 0.3
    # AD_thresh_high = 0.5
    AD_thresh_high = 0.36

    pred_fa = FA_model.predict(arr_frame) #numerical FA risk assessment
    #FA thresholds for each category (low/ medium/ high)
    # FA_thresh_low = 0.368
    # FA_thresh_high = 0.55
    FA_thresh_high = 0.27

    if skintype == "Black":
        pred_ad+= -0.1
        pred_fa += -0.1
    if breastfeeding:
        # pred_ad+= 0.05
        pred_fa += 0.05

    #calculate AD verbal assessment
    if pred_ad > AD_thresh_high:
        ad_risk = "high"
    # elif pred_ad > AD_thresh_low:
    #     ad_risk = "moderate"
    else:
        ad_risk = "low"

    mom_history= (FAmom or ADmom or asthma_mom)
    dad_history= (FAdad or ADdad or asthma_dad)
    sib_history= (fever_sibiling or FAsibiling or ADsibiling)
    
    # if (ad_risk=="high" and not (mom_history or dad_history)):
    #     ad_risk = "moderate"
    if (ad_risk=="low" and (mom_history and dad_history)):
        # ad_risk = "moderate"
        ad_risk = "high"
    if (ad_risk=="low" and sib_history):
        # ad_risk = "moderate"
        ad_risk = "high"

    #calculate FA verbal assessment
    if pred_fa > FA_thresh_high:
        fa_risk = "high"
    # elif pred_fa > FA_thresh_low:
    #     fa_risk = "moderate"
    else:
        fa_risk = "low"

    # if (fa_risk == "high" and not (mom_history or dad_history)):
    #     fa_risk = "moderate"
    if (fa_risk == "low" and (mom_history and dad_history)):
        # fa_risk = "moderate"
        fa_risk = "high"
    if (fa_risk == "low" and sib_history):
        # fa_risk = "moderate"
        fa_risk = "high"

    # if (fa_risk == "high" and ad_risk=="low"):
    #     ad_risk="moderate"
    # if (ad_risk == "high" and fa_risk=="low"):
    #     fa_risk="moderate"
        
    categories= ["skin", "infant", "history", "prgnancy", "settings", "future"]
    print('\033[1m' + "score_ad ", pred_ad,
          "risk_ad", ad_risk)
    print('\033[1m' + "score_fa ", pred_fa,
          "risk_fa", fa_risk)

    FAData = []
    ADData = []

    FAData.append(sign(sum(fa_dict["skin"])))  # FA TEWL
    FAData.append(sign(sum(fa_dict["infant"])))  # FA infant
    FAData.append(sign(sum(fa_dict["history"])))  # FA history
    FAData.append(sign(sum(fa_dict["pregnancy"])))  # FA preg
    FAData.append(sign(sum(fa_dict["settings"])))  # FA settings
    FAData.append(sign(sum(fa_dict["future"])))  # FA future

    ADData.append(sign(sum(ad_dict["skin"])))  # AD TEWL
    ADData.append(sign(sum(ad_dict["infant"])))  # AD infant
    ADData.append(sign(sum(ad_dict["history"])))  # AD history
    ADData.append(sign(sum(ad_dict["pregnancy"])))  # AD preg
    ADData.append(sign(sum(ad_dict["settings"])))  # AD settings
    ADData.append(sign(0))  # AD future


    print("score_ad ", pred_ad,
          "risk_ad", ad_risk,
          "score_fa ", pred_fa,
          "risk_fa", fa_risk,
          "FAData", FAData,
          "ADData", ADData
    )

    return {
            # "score_ad ": pred_ad,
            "AD": ad_risk,
            # "score_fa ": pred_fa,
            "FA": fa_risk,
            "FAData":FAData,
            "ADData":ADData

        }


if __name__ == "__main__":
    # event={
    #       "TEWL": 5,
    #       "month": 5,
    #       "day": 5,
    #       "gender": "Female",
    #       "gestational": 38,
    #       "isCaesarean": True,
    #       "weigh": 2500,
    #       "momBirth": 1992,
    #       "siblings": 2,
    #       "isMomSmoke": False,
    #       "momCondition": [],
    #       "fatherCondition": [],
    #       "siblingsCondition": [],
    #       "isBreastfeed": False,
    #       "isCat": True,
    #       "includeVitaminD": True,
    #       "childCareTimeStart": "Month6",
    #       "liveSetting": "Urban",
    #       "isDog": True,
    #       "isMomExposedFarmAnimals": True,
    #       "skin": "White"
    #     }
    main('', '')
