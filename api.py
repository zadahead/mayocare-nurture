from flask import Flask, request, jsonify, make_response
from flask_restful import Resource, Api
from flask_cors import CORS

from process import main as Prediction;

app = Flask(__name__)
CORS(app)
api = Api(app)



class PredictionAPI(Resource):
    def __init__(self):
        super().__init__()

    def post(self):
        try:
            json_data = request.get_json(force=True)
           
            output = Prediction(json_data)

            return make_response(jsonify(output), 200)

        except Exception as err:
            return make_response(jsonify({"message": str(err)}), 500)


api.add_resource(PredictionAPI, '/api/v2/prediction')

            

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8000, debug=True)
