# Server Setup and Model Deployment

Dependencies:

1. Python
2. Docker
3. Supervisor

How to Setup:

1. Clone the repo `https://github.com/Sarvesh1523/myropar-deployment.git`.
2. Run `cd myropar-deployment`.
3. Run `source setup.sh` to setup Python, Pip, Build-Essential, Docker, Supervisor and deploy the existing code.

Model is deployed and ready to serve.
