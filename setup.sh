#!/bin/bash

# Setup Python, pip and build-essential
sudo apt -y update
sudo apt -y upgrade
sudo apt install -y python3-pip
sudo apt install -y build-essential libssl-dev libffi-dev python3-dev

# Setup Docker
sudo apt update
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt update
sudo apt-cache policy docker-ce
sudo apt install -y docker-ce

# Build Docker Image
sudo docker build -t myropar/deployment:flask .
sudo docker run -d -p 8000:8000 -t myropar/deployment:flask

# Setup Supervisor
sudo mkdir /var/log/deployment
sudo touch /var/log/deployment/docker_info.log
sudo touch /var/log/deployment/docker_error.log
sudo apt-get install supervisor -y
supervisord -v
sudo cp supervisor.conf /etc/supervisor/conf.d/
sudo supervisorctl reload
sudo docker ps
